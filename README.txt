HiddenBlock Atto plugin
============================

The hidden block plugin combined with the filter allows for a section of content
to be hidden when the content is rendered to the screen for the student.