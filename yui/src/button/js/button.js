// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * @package    atto_hiddenblock
 * @copyright  University of Wisconsin - Madison
 * @author     John Hoopes <john.hoopes@wisc.edu>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * @module moodle-atto_hiddenblock-button
 */

/**
 * Atto text editor hiddenblock plugin.
 *
 * @namespace M.atto_hiddenblock
 * @class button
 * @extends M.editor_atto.EditorPlugin
 */


Y.namespace('M.atto_hiddenblock').Button = Y.Base.create('button', Y.M.editor_atto.EditorPlugin, [], {

  
	/**
     * Initialize the button
     *
     * @method Initializer
     */
    initializer: function() {
        // If we don't have the capability to view then give up.
        if (this.get('disabled')){
            return;
        }

        this.addButton({
            icon: 'icon',
            iconComponent: 'atto_hiddenblock',
            buttonName: 'hiddenblock',
            callback: this._insertHiddenBlock
        });

    },


     /**
     * Insert's the hiddenblock onto the page
     *
     *
     * @private
     */
    _insertHiddenBlock: function(e) {
        e.preventDefault();

        var selection = window.rangy.getSelection();
         var host = this.get('host');

         if(selection.rangeCount > 1){
             return;
         }

         var range = selection.getRangeAt(0);

         // next check for pre-existing hiddenblock nodes
         // if there is a hiddenblock node within the selection, notify the user that they cannot nest hiddenblocks
         if(this._checkForHiddenBlocks(range)){
             host.showMessage('You cannot have nested Hidden Blocks', 'warning', 5000);
             return;
         }

        // if the range is empty just add an empty hiddenblock element
         if(range.toHtml() === ''){
             var hiddenBlock = this._buildHiddenBlock('');
             range.insertNode(hiddenBlock);
         }else{
             var hiddenblock = '';

             if(this._getParentContainer(range) !== host.editor.getDOMNode()){
                 // if the parent Container for the range isn't the same as the host editor, we'll wrap the parent node (common anscestor)
                 // with a hidden block div

                 //this.editor.log('parent node' + this._getParentContainer(range));
                 host.showMessage('the parent node doesnt match', 'info', 5000);
                 hiddenblock = this._buildHiddenBlock(this._getParentContainer(range));

             }else{
                 // we'll wrap the range itself with the hidden block div
                 host.showMessage('parent nodes and editor host matches', 'info', 5000);
                 hiddenblock = this._buildHiddenBlock(range.toHtml());

             }

             range.deleteContents();
             range.insertNode(hiddenblock);
         }
    },

    _getParentContainer: function( range ) {

        if( typeof(msie) !== 'undefined'){
            return range.parentElement();
        }else{
            return range.commonAncestorContainer;
        }
    },



    _checkForHiddenBlocks: function(range){

        var host = this.get('host');
        var allNodes = host.getSelectedNodes();

        var result = false;
        allNodes.each(function(node){
            var domNode = node.getDOMNode();
            if(domNode.tagName === 'DIV' && domNode.classList.contains('hiddenblock')){
                result = true;
            }
        });

        var node = range.startContainer;

        while(node){
            var parent = node.parentNode;
            if(parent === null || (parent.classList.contains('editor_atto_content_wrap'))){
                node = false;
            }else{
                if(parent.tagName === 'DIV' && parent.classList.contains('hiddenblock')){
                    result = true;
                }
                node = parent;
            }
        }


        return result;
    },

    _buildHiddenBlock: function(contents){

        var hiddenblock = document.createElement('div');
        hiddenblock.classList.add('hiddenblock');

        if(this._isNode(contents)){
            hiddenblock.appendChild(contents);
        }else{
            if(contents === ''){ // if empty contents add a blank paragraph tag to fill out the hiddenblock div
                contents = '<p>&nbsp;</p>';
            }

            hiddenblock.innerHTML = contents;
        }

        return hiddenblock;
    },

    _isNode: function(o){
        return (
            typeof Node === "object" ? o instanceof Node :
            o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName==="string"
        );
    }


}, { ATTRS: {
		disabled: {
			value: false
		},

		usercontextid: {
			value: null
		}
	}
});
